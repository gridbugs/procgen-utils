use args::{Arg, Args};
use grid_2d::{coord_2d::Axis, Coord, CoordIter, Grid, Size};
use rand::Rng;

#[derive(Clone, Copy)]
struct BspConfig {
    min_room_size: Size,
}

#[derive(Clone)]
enum Cell {
    Wall,
    Floor,
}

#[derive(Debug)]
struct Rect {
    coord: Coord,
    size: Size,
}

fn leaf(rect: Rect, grid: &mut Grid<Cell>) {
    for rect_coord in CoordIter::new(rect.size) {
        let grid_coord = rect_coord + rect.coord;
        *grid.get_checked_mut(grid_coord) = Cell::Floor;
    }
}

fn generate<R: Rng>(rect: Rect, config: BspConfig, grid: &mut Grid<Cell>, rng: &mut R) {
    let axis_to_split = if rect.size.width() < config.min_room_size.width() * 2 + 1 {
        if rect.size.height() < config.min_room_size.height() * 2 + 1 {
            return leaf(rect, grid);
        } else {
            Axis::Y
        }
    } else {
        if rect.size.height() < config.min_room_size.height() * 2 + 1 {
            Axis::X
        } else {
            if rng.gen() {
                Axis::X
            } else {
                Axis::Y
            }
        }
    };
    let size_in_axis = rect.size.get(axis_to_split);
    let min_size_in_axis = config.min_room_size.get(axis_to_split);
    let max_offset_to_split = size_in_axis - (min_size_in_axis * 2);
    assert!(max_offset_to_split > 0);
    let offset_to_split = rng.gen_range(0, max_offset_to_split);
    let left_rect = Rect {
        coord: rect.coord,
        size: rect.size.set(axis_to_split, min_size_in_axis + offset_to_split),
    };
    let right_rect = Rect {
        coord: rect.coord + Coord::new(0, 0).set(axis_to_split, (min_size_in_axis + offset_to_split + 1) as i32),
        size: rect
            .size
            .set(axis_to_split, size_in_axis - (min_size_in_axis + offset_to_split + 1)),
    };
    assert!(left_rect.size.width() >= config.min_room_size.width());
    assert!(left_rect.size.height() >= config.min_room_size.height());
    assert!(right_rect.size.width() >= config.min_room_size.width());
    assert!(right_rect.size.height() >= config.min_room_size.height());
    generate(left_rect, config, grid, rng);
    generate(right_rect, config, grid, rng);
}

fn main() {
    let Args { mut rng, size } = Args::arg().with_help_default().parse_env_or_exit();
    let mut grid = Grid::new_clone(size, Cell::Wall);
    let config = BspConfig {
        min_room_size: Size::new_u16(4, 4),
    };
    generate(
        Rect {
            coord: Coord::new(1, 1),
            size: grid.size() - Size::new_u16(2, 2),
        },
        config,
        &mut grid,
        &mut rng,
    );
    for row in grid.rows() {
        for cell in row {
            let ch = match cell {
                Cell::Wall => '#',
                Cell::Floor => '.',
            };
            print!("{}", ch);
        }
        println!("");
    }
}
