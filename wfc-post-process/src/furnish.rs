use crate::connections::ConnectedCell;
use grid_2d::Grid;
use rand::Rng;

pub enum FurnishedCell {
    Wall,
    Floor,
    Space,
    Door,
    Window,
    Star,
}

pub fn add_furniture<R: Rng>(grid: &Grid<ConnectedCell>, rng: &mut R) -> Grid<FurnishedCell> {
    Grid::new_grid_map_ref(grid, |cell| match cell {
        ConnectedCell::Door => FurnishedCell::Door,
        ConnectedCell::Wall => FurnishedCell::Wall,
        ConnectedCell::Floor => FurnishedCell::Floor,
        ConnectedCell::Window => FurnishedCell::Window,
        ConnectedCell::Space => {
            if rng.gen_range(0, 20) == 0 {
                FurnishedCell::Star
            } else {
                FurnishedCell::Space
            }
        }
    })
}
