use args::{Arg, Args};

mod hull;
use hull::generate_hull;

mod internal_walls;
use internal_walls::add_internal_walls;

mod connections;
use connections::add_connections;

mod furnish;
use furnish::{add_furniture, FurnishedCell};

fn main() {
    let Args {
        mut rng,
        size: output_size,
    } = Args::arg().with_help_default().parse_env_or_exit();
    let space_width = 4;
    let hull = generate_hull(output_size, space_width, &mut rng);
    let output_grid = add_internal_walls(&hull, &mut rng);
    let output_grid = add_connections(&output_grid, &mut rng);
    let output_grid = add_furniture(&output_grid, &mut rng);
    println!("    abcdefghijklmnopqrstuvwxyz");
    for (i, rows) in output_grid.rows().enumerate() {
        print!("{:2}: ", i);
        for cell in rows {
            let ch = match cell {
                FurnishedCell::Wall => '#',
                FurnishedCell::Floor => '.',
                FurnishedCell::Space => ' ',
                FurnishedCell::Door => '+',
                FurnishedCell::Window => '=',
                FurnishedCell::Star => '.',
            };
            print!("{}", ch);
        }
        println!("");
    }
}
